import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ButtonsComponent} from "./components/buttons/buttons.component";
import {RouteConstants} from "../../utility/constants/routes";
import {TablesComponent} from "./components/tables/tables.component";
import {InputsComponent} from "./forms/inputs/inputs.component";
import {TextareaComponent} from "./forms/textarea/textarea.component";
import {CheckboxComponent} from "./forms/checkbox/checkbox.component";
import {RadioComponent} from "./forms/radio/radio.component";
import {SelectComponent} from "./forms/select/select.component";
import {InputFilesComponent} from "./forms/input-files/input-files.component";
import {HeaderPanelComponent} from "./navigation/header-panel/header-panel.component";
import {SidebarComponent} from "./navigation/sidebar/sidebar.component";
import {SwitchComponent} from "./forms/switch/switch.component";
import {BordersComponent} from "./utilities/borders/borders.component";
import {FooterComponent} from "./navigation/footer/footer.component";
import {HomeComponent} from "./home/home.component";
import {ModalComponent} from "./components/modal/modal.component";
import {LoginComponent} from "./pages/login/login.component";
import {AccordionComponent} from "./components/accordion/accordion.component";
import {TabsComponent} from "./components/tabs/tabs.component";
import {FloatsComponent} from "./utilities/floats/floats.component";
import {StepperComponent} from "./components/stepper/stepper.component";

const routes: Routes = [
  {
    path: RouteConstants.HOME,
    component: HomeComponent
  },
  {
    path: RouteConstants.BUTTONS,
    component: ButtonsComponent
  },
  {
    path: RouteConstants.TABLES,
    component: TablesComponent
  },
  {
    path: RouteConstants.INPUTS,
    component: InputsComponent
  },
  {
    path: RouteConstants.INPUTS_FILES,
    component: InputFilesComponent
  },
  {
    path: RouteConstants.SELECT,
    component: SelectComponent
  },
  {
    path: RouteConstants.TEXTAREA,
    component: TextareaComponent
  },
  {
    path: RouteConstants.CHECKBOX,
    component: CheckboxComponent
  },
  {
    path: RouteConstants.RADIO,
    component: RadioComponent
  },
  {
    path: RouteConstants.SWITCH,
    component: SwitchComponent
  },
  {
    path: RouteConstants.HEADER,
    component: HeaderPanelComponent
  },
  {
    path: RouteConstants.SIDEBAR,
    component: SidebarComponent
  },
  {
    path: RouteConstants.BORDERS,
    component: BordersComponent
  },
  {
    path: RouteConstants.FOOTER,
    component: FooterComponent
  },
  {
    path: RouteConstants.MODAL,
    component: ModalComponent
  },
  {
    path: RouteConstants.LOGIN,
    component: LoginComponent
  },
  {
    path: RouteConstants.ACCORDION,
    component: AccordionComponent
  },
  {
    path: RouteConstants.TABS,
    component: TabsComponent
  },
  {
    path: RouteConstants.STEPPER,
    component: StepperComponent
  },
  {
    path: RouteConstants.FLOATS,
    component: FloatsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule {

}
