import {Component, OnInit} from '@angular/core';
import {RouteConstants} from "../../../utility/constants/routes";
import {Router} from "@angular/router";
import {BaseComponent} from "../../../utility/base-component/base.component";

@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html'
})
export class HeaderComponent extends BaseComponent implements OnInit {

  // state variables
  isOpenMenu: any = false;

  constructor(private router: Router) {
    super();
  }

  ngOnInit() {
  }

  // Events
  onOpenMenu(name) {
    this.isOpenMenu === name ? this.isOpenMenu = '' : this.isOpenMenu = name;
  }

  hideInnerMenu(){
    this.isOpenMenu = false;
  }

  get buttonsUrl() {
    return ['/' + RouteConstants.BUTTONS];
  }

  get tablesUrl() {
    return ['/' + RouteConstants.TABLES];
  }

  get modalUrl() {
    return ['/' + RouteConstants.MODAL];
  }

  get accordionUrl() {
    return ['/' + RouteConstants.ACCORDION];
  }

  get tabsUrl() {
    return ['/' + RouteConstants.TABS];
  }

  get stepperUrl() {
    return ['/' + RouteConstants.STEPPER];
  }

  get inputsUrl() {
    return ['/' + RouteConstants.INPUTS];
  }

  get inputsFilesUrl() {
    return ['/' + RouteConstants.INPUTS_FILES];
  }

  get selectUrl() {
    return ['/' + RouteConstants.SELECT];
  }

  get textareaUrl() {
    return ['/' + RouteConstants.TEXTAREA];
  }

  get checkboxUrl() {
    return ['/' + RouteConstants.CHECKBOX];
  }

  get radioUrl() {
    return ['/' + RouteConstants.RADIO];
  }

  get switchUrl() {
    return ['/' + RouteConstants.SWITCH];
  }

  get headerUrl() {
    return ['/' + RouteConstants.HEADER];
  }

  get sidebarUrl() {
    return ['/' + RouteConstants.SIDEBAR];
  }

  get footerUrl() {
    return ['/' + RouteConstants.FOOTER];
  }

  get bordersUrl() {
    return ['/' + RouteConstants.BORDERS];
  }

  get floatsUrl() {
    return ['/' + RouteConstants.FLOATS];
  }

  get loginUrl() {
    return ['/' + RouteConstants.LOGIN];
  }
}
