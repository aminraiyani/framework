import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "../../../utility/base-component/base.component";

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html'
})
export class TablesComponent extends BaseComponent implements OnInit {

  type: any = 'basic';
  basicTable: string;
  noBorderedTable: string;
  borderedTable: string;
  theadTable: string;
  stripedTable: string;
  stripedBorderTable: string;
  centerTable: string;
  hoverTable: string;

  constructor() {
    super();
  }

  ngOnInit() {
    setTimeout(() => {
      this.basicTable = this.querySelector('table');
    }, 10)
  }

  onNavigationChange(event) {
    this.type = event;

    if (event == 'thead') {
      setTimeout(() => {
        this.theadTable = this.querySelector('table-thead');
      }, 10)
    }

    if (event == 'noBordered') {
        setTimeout(() => {
          this.noBorderedTable = this.querySelector('table-no-bordered');
        }, 10)
      }

    if (event == 'bordered') {
      setTimeout(() => {
        this.borderedTable = this.querySelector('table-bordered');
      }, 10)
    }

    if (event == 'striped') {
      setTimeout(() => {
        this.stripedTable = this.querySelector('table-striped');
      }, 10)
    }

    if (event == 'stripedBorder') {
      setTimeout(() => {
        this.stripedBorderTable = this.querySelector('table-striped-border');
      }, 10)
    }

    if (event == 'center') {
      setTimeout(() => {
        this.centerTable = this.querySelector('table-center');
      }, 10)
    }

    if (event == 'hover') {
      setTimeout(() => {
        this.hoverTable = this.querySelector('table-hover');
      }, 10)
    }
  }
}
