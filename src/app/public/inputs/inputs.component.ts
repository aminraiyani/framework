import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "../../../utility/base-component/base.component";

@Component({
  selector: 'app-inputs',
  templateUrl: './inputs.component.html'
})
export class InputsComponent extends BaseComponent implements OnInit {

  type: any = 'basic';

  inputsArray = [
    {
      classNew: '',
      className: 'basic-input',
    },
    {
      classNew: 'basic-input',
      className: 'basic-input-border'
    }
  ];

  constructor() {
    super();
  }

  ngOnInit() {
  }

  getClass(inputs) {
    let className = '';
    if (inputs.classNew) {
      className = inputs.classNew;
    }
    if (inputs.className) {
      if (className) {
        className = className + ' ' + inputs.className;
      } else {
        className = inputs.className;
      }
    }
    return className
  }

  onNavigationChange(event) {
    this.type = event;
  }
}
