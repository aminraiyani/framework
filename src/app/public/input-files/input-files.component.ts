import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "../../../utility/base-component/base.component";

@Component({
  selector: 'app-input-files',
  templateUrl: './input-files.component.html'
})

export class InputFilesComponent extends BaseComponent implements OnInit {

  type: any = 'basic';
  fileName: string;

  flatButton: any;

  htmlString = '<input type="file" hidden id="file" (change)="onFileGetName($event)">' + '\n' +
    '<input type="text" placeholder="Select file" readonly (click)="onFileSelect(' + 'file' + ')" value="{{fileName}}"/>' + '\n' +
    '<label>Passport</label>' + '\n' +
    '<i class="fa fa-paperclip"></i>';

  typeScriptString = 'fileName: string;' + '\n' +
    'onFileSelect(id) {' + '\n' +
    'document.getElementById(id).click();' + '\n' +
    '}' + '\n' +
    'onFileGetName(event) {' + '\n' +
    'const file = event.target.files[0]; ' + '\n' +
    'this.fileName = file.name; ' + '\n' +
    '}';

  constructor() {
    super();
  }


  ngOnInit() {
    setTimeout(() => {
      this.flatButton = this.querySelector('basic-input');
    }, 100)
  }


  onNavigationChange(event) {
    this.type = event;
  }

  onFileSelect(id) {
    document.getElementById(id).click();
  }

  onFileGetName(event) {
    const file = event.target.files[0];
    this.fileName = file.name;
  }
}
