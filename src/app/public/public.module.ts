import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PublicRoutingModule} from './public-routing.module';
import {ButtonsComponent} from "./components/buttons/buttons.component";
import {UtilityModule} from "../../utility/utility.module";
import {HeaderComponent} from './header/header.component';
import {TablesComponent} from './components/tables/tables.component';
import {InputsComponent} from './forms/inputs/inputs.component';
import {TextareaComponent} from './forms/textarea/textarea.component';
import {CheckboxComponent} from './forms/checkbox/checkbox.component';
import {RadioComponent} from './forms/radio/radio.component';
import {SelectComponent} from './forms/select/select.component';
import {InputFilesComponent} from './forms/input-files/input-files.component';
import {MarkdownModule} from "ngx-markdown";
import {HeaderPanelComponent} from './navigation/header-panel/header-panel.component';
import {SidebarComponent} from './navigation/sidebar/sidebar.component';
import {SwitchComponent} from './forms/switch/switch.component';
import {BordersComponent} from './utilities/borders/borders.component';
import {FooterComponent} from './navigation/footer/footer.component';
import {HomeComponent} from './home/home.component';
import {ModalComponent} from './components/modal/modal.component';
import {LoginComponent} from './pages/login/login.component';
import {AccordionComponent} from './components/accordion/accordion.component';
import {TabsComponent} from './components/tabs/tabs.component';
import {FloatsComponent} from './utilities/floats/floats.component';
import {VerticalAlignComponent} from './utilities/vertical-align/vertical-align.component';
import {TextComponent} from './utilities/text/text.component';
import {StepperComponent} from './components/stepper/stepper.component';

@NgModule({
  imports: [
    CommonModule,
    UtilityModule,
    MarkdownModule.forRoot(),
    PublicRoutingModule
  ],
  declarations: [
    ButtonsComponent,
    HeaderComponent,
    TablesComponent,
    InputsComponent,
    TextareaComponent,
    CheckboxComponent,
    RadioComponent,
    SelectComponent,
    InputFilesComponent,
    HeaderPanelComponent,
    SidebarComponent,
    SwitchComponent,
    BordersComponent,
    FooterComponent,
    HomeComponent,
    ModalComponent,
    LoginComponent,
    AccordionComponent,
    TabsComponent,
    FloatsComponent,
    VerticalAlignComponent,
    TextComponent,
    StepperComponent
  ],
  exports: [
    HeaderComponent
  ]
})
export class PublicModule {
}
