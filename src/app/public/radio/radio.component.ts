import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "../../../utility/base-component/base.component";

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html'
})
export class RadioComponent extends BaseComponent implements OnInit {

  radioButtonCode: string;

  constructor() {
    super();
  }

  ngOnInit() {
    setTimeout(() => {
      this.radioButtonCode = this.querySelector('radio-group');
    }, 10)
  }

}
