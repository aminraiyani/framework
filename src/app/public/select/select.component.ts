import { Component, OnInit } from '@angular/core';
import {BaseComponent} from "../../../utility/base-component/base.component";

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html'
})
export class SelectComponent extends BaseComponent implements OnInit {

  type: any = 'basic';

  selectsArray = [
    {
      classNew: '',
      className: 'basic-select',
    },
    {
      classNew: 'basic-select',
      className: 'basic-select-border'
    }
  ];

  constructor() {
    super();
  }

  ngOnInit() {
  }

  getClass(inputs) {
    let className = '';
    if (inputs.classNew) {
      className = inputs.classNew;
    }
    if (inputs.className) {
      if (className) {
        className = className + ' ' + inputs.className;
      } else {
        className = inputs.className;
      }
    }
    return className
  }

  onNavigationChange(event) {
    this.type = event;
  }
}
