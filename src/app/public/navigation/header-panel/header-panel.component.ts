import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "../../../../utility/base-component/base.component";
import {SharedService} from "../../../../utility/shared.service";
import {BASE} from "../../../../utility/constants/base-constants";

@Component({
  selector: 'app-header-panel',
  templateUrl: 'header-panel.component.html'
})
export class HeaderPanelComponent extends BaseComponent implements OnInit {

  // state variables
  isDropdown = false;

  baseFile = new BASE();

  files = [
    {
      filePath: this.baseFile.FILE_PATH + 'header-files/header.js',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'header-files/header.html',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'header-files/header-icons.html',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'header-files/white-header.html',
      codeText: ''
    }
  ];

  constructor(private _sharedService: SharedService) {
    super()
  }

  ngOnInit() {
    this.initializeMethod();
  }

  initializeMethod() {
    for (let i = 0; i < this.files.length; i++) {
      this._sharedService.onCopyFile(this.files[i].filePath).subscribe(res=> {
        this.files[i].codeText = res;
      });
    }
  }
}


