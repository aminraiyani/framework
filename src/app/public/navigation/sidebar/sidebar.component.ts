import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "../../../../utility/base-component/base.component";
import {BASE} from "../../../../utility/constants/base-constants";
import {SharedService} from "../../../../utility/shared.service";

@Component({
  selector: 'app-sidebar',
  templateUrl: 'sidebar.component.html'
})
export class SidebarComponent extends BaseComponent implements OnInit {

  baseFile = new BASE();

  isOpenMenu: any = false;
  isSidebarMove = false;
  isExpand = false;

  files = [
    {
      filePath: this.baseFile.FILE_PATH + 'sidebar-files/sidebarBasic.html',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'sidebar-files/sidebarBasic.js',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'sidebar-files/drop-menu-sidebar.html',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'sidebar-files/drop-menu-sidebar.js',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'sidebar-files/hover-menu-sidebar.html',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'sidebar-files/hover-menu-sidebar.js',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'sidebar-files/small-sidebar.html',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'sidebar-files/small-sidebar.js',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'sidebar-files/toggle-sidebar.html',
      codeText: ''
    }
  ];

  constructor(private _sharedService: SharedService) {
    super();
  }

  ngOnInit() {
    this.initializeMethod();
  }

  initializeMethod() {
    for (let i = 0; i < this.files.length; i++) {
      this._sharedService.onCopyFile(this.files[i].filePath).subscribe(res=> {
        this.files[i].codeText = res;
      });
    }
  }

  // Events
  onOpenMenu(name) {
    this.isOpenMenu === name ? this.isOpenMenu = '' : this.isOpenMenu = name;
  }

  hideMenu() {
    this.isSidebarMove = false;
  }

  onSidebarHover() {
    this.isSidebarMove = true;
  }

  menuToggle() {
    this.isExpand = !this.isExpand;
  }
}
