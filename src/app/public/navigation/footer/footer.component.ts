import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "../../../../utility/base-component/base.component";
import {SharedService} from "../../../../utility/shared.service";
import {BASE} from "../../../../utility/constants/base-constants";

@Component({
  selector: 'app-footer',
  templateUrl: 'footer.component.html'
})
export class FooterComponent extends BaseComponent implements OnInit {

  baseFile = new BASE();

  files = [
    {
      filePath: this.baseFile.FILE_PATH + 'footer/basic-footer.html',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'footer/basic-footer.js',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'footer/social-footer.html',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'footer/white-footer.html',
      codeText: ''
    }
  ];

  constructor(private _sharedService: SharedService) {
    super();
  }


  ngOnInit() {
    this.initializeMethod();
  }

  initializeMethod() {
    for (let i = 0; i < this.files.length; i++) {
      this._sharedService.onCopyFile(this.files[i].filePath).subscribe(res=> {
        this.files[i].codeText = res;
      });
    }
  }
}
