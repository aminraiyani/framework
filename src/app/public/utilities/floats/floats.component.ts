import { Component, OnInit } from '@angular/core';
import {BaseComponent} from "../../../../utility/base-component/base.component";

@Component({
  selector: 'app-floats',
  templateUrl: './floats.component.html'
})
export class FloatsComponent extends BaseComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
