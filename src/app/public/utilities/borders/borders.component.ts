import { Component, OnInit } from '@angular/core';
import {BaseComponent} from "../../../../utility/base-component/base.component";

@Component({
  selector: 'app-borders',
  templateUrl: 'borders.component.html'
})
export class BordersComponent extends BaseComponent implements OnInit {

  borderArray = [
    {
      className: 'top-border'
    },
    {
      className: 'right-border'
    },
    {
      className: 'bottom-border'
    },
    {
      className: 'left-border'
    }
  ];

  borderRadiusArray = [
    {
      className: 'rounded'
    },
    {
      className: 'rounded-circle'
    },
    {
      className: 'no-rounded'
    }
  ];

  borderColorArray = [
    {
      className: 'primary-border'
    },
    {
      className: 'secondary-border'
    },
    {
      className: 'tertiary-border'
    },
    {
      className: 'light-grey-border'
    },
    {
      className: 'dark-grey-border'
    },
    {
      className: 'no-border'
    }
  ];

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
