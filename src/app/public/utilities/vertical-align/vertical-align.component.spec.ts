import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerticalAlignComponent } from './vertical-align.component';

describe('VerticalAlignComponent', () => {
  let component: VerticalAlignComponent;
  let fixture: ComponentFixture<VerticalAlignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerticalAlignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerticalAlignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
