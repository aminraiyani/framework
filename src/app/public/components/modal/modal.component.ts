import {Component, OnInit, HostListener} from '@angular/core';
import {BaseComponent} from "../../../../utility/base-component/base.component";
import {BASE} from "../../../../utility/constants/base-constants";
import {SharedService} from "../../../../utility/shared.service";

@Component({
  selector: 'app-modal',
  templateUrl: 'modal.component.html'
})
export class ModalComponent extends BaseComponent implements OnInit {

  baseFile = new BASE();

  /*state variables*/
  basicModal = false;
  smallModal = false;
  largeModal = false;

  files = [
    {
      filePath: this.baseFile.FILE_PATH + 'modal-files/basic-modal.html',
      codeText: ''
    }, {
      filePath: this.baseFile.FILE_PATH + 'modal-files/basic-modal.js',
      codeText: ''
    }, {
      filePath: this.baseFile.FILE_PATH + 'modal-files/small-modal.html',
      codeText: ''
    }, {
      filePath: this.baseFile.FILE_PATH + 'modal-files/small-modal.js',
      codeText: ''
    }, {
      filePath: this.baseFile.FILE_PATH + 'modal-files/large-modal.html',
      codeText: ''
    }, {
      filePath: this.baseFile.FILE_PATH + 'modal-files/large-modal.js',
      codeText: ''
    }
  ];

  constructor(private _sharedService: SharedService) {
    super();
  }

  ngOnInit() {
    this.initializeMethod();
  }

  initializeMethod() {
    for (let i = 0; i < this.files.length; i++) {
      this._sharedService.onCopyFile(this.files[i].filePath).subscribe(res=> {
        this.files[i].codeText = res;
      });
    }
  }

  /*Events*/
  onShowBasicModal(id) {
    this.basicModal = true;
    this.smallModal = true;
    this.largeModal = true;
    this.onOpenModal(id);
  }

  onCloseModal() {
    this.onRemoveScroll();
    this.basicModal = false;
    this.smallModal = false;
    this.largeModal = false;
  }

  // Helpers
  @HostListener('window:keydown', ['$event'])
  keyboardInput(event: any) {
    if (event.keyCode === 27) {
      this.onCloseModal();
    }
  }
}
