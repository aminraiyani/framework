import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "../../../../utility/base-component/base.component";
import {SharedService} from "../../../../utility/shared.service";
import {BASE} from "../../../../utility/constants/base-constants";

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html'
})
export class AccordionComponent extends BaseComponent implements OnInit {

  baseFile = new BASE();

  // State variables
  isOpen = -1;

  files = [
    {
      filePath: this.baseFile.FILE_PATH + 'accordion-files/basic-accordion.html',
      codeText: ''
    }, {
      filePath: this.baseFile.FILE_PATH + 'accordion-files/basic-accordion.js',
      codeText: ''
    }
  ];

  constructor(private _sharedService: SharedService) {
    super();
  }

  ngOnInit() {
    this.initializeMethod();
  }

  initializeMethod() {
    for (let i = 0; i < this.files.length; i++) {
      this._sharedService.onCopyFile(this.files[i].filePath).subscribe(res=> {
        this.files[i].codeText = res;
      });
    }
  }

  // Events
  openAccordion(i) {
    (this.isOpen == i) ? (this.isOpen = -1) : (this.isOpen = i);
  }
}
