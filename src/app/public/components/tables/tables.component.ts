import {Component, OnInit} from '@angular/core';
import {BASE} from "../../../../utility/constants/base-constants";
import {BaseComponent} from "../../../../utility/base-component/base.component";
import {SharedService} from "../../../../utility/shared.service";

@Component({
  selector: 'app-tables',
  templateUrl: 'tables.component.html'
})
export class TablesComponent extends BaseComponent implements OnInit {

  baseFile = new BASE();

  // State variables
  sortKey = 'username';
  isSort = false;

  isExpandRow = -1;

  files = [
    {
      filePath: this.baseFile.FILE_PATH + 'table-files/sorting-table.html',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'table-files/sorting-table.js',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'table-files/expandable-table.html',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'table-files/expandable-table.js',
      codeText: ''
    }
  ];

  constructor(private _sharedService: SharedService) {
    super();
  }

  ngOnInit() {
    this.initializeMethod();
  }

  initializeMethod() {
    for (let i = 0; i < this.files.length; i++) {
      this._sharedService.onCopyFile(this.files[i].filePath).subscribe(res=> {
        this.files[i].codeText = res;
      });
    }
  }

  // Events
  onSorting(keyName) {
    this.sortKey === keyName ? (this.isSort = !this.isSort) : this.isSort = true;
    this.sortKey = keyName;
  }

  openRow(i){
    this.isExpandRow = (this.isExpandRow != i) ? i : -1;
  }
}
