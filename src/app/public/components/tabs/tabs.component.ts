import { Component, OnInit } from '@angular/core';
import {SharedService} from "../../../../utility/shared.service";
import {BASE} from "../../../../utility/constants/base-constants";
import {BaseComponent} from "../../../../utility/base-component/base.component";

enum View {
  TAB_ONE, TAB_TWO, TAB_THREE
}

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html'
})
export class TabsComponent extends BaseComponent implements OnInit {

  baseFile = new BASE();

  // State variables
  enumView = View;
  activeView : View = View.TAB_ONE;

  files = [
    {
      filePath: this.baseFile.FILE_PATH + 'tabs-files/basic-tabs.html',
      codeText: ''
    }, {
      filePath: this.baseFile.FILE_PATH + 'tabs-files/basic-tabs.js',
      codeText: ''
    }
  ];

  constructor(private _sharedService: SharedService) {
    super();
  }

  ngOnInit() {
    this.initializeMethod();
  }

  initializeMethod() {
    for (let i = 0; i < this.files.length; i++) {
      this._sharedService.onCopyFile(this.files[i].filePath).subscribe(res=> {
        this.files[i].codeText = res;
      });
    }
  }

  // Events
  onOpenTab(viewName : View){
    this.activeView = viewName;
  }
}
