import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "../../../../utility/base-component/base.component";

@Component({
  selector: 'app-buttons',
  templateUrl: 'buttons.component.html'
})
export class ButtonsComponent extends BaseComponent implements OnInit {

  loadingButtonArray = [
    {
      title: 'Loading',
      className: 'btn-primary',
      iconClassName: 'fa-spinner'
    },
    {
      title: 'Configuring',
      className: 'btn-secondary',
      iconClassName: 'fa-cog'
    }
  ];

  iconAnimationArray = [
    {
      title: 'Submit',
      className: 'btn-primary',
      iconClassName: 'fa-chevron-right'
    },
    {
      title: 'Submit',
      className: 'btn-secondary',
      iconClassName: 'fa-arrow-right'
    }
  ];

  iconButtonArray = [
    {
      title: 'Add',
      basicClass: 'btn-primary',
      iconClassName: 'fa-plus'
    },
    {
      title: 'Edit',
      basicClass: 'btn-secondary',
      iconClassName: 'fa-edit'
    },
    {
      title: 'Delete',
      basicClass: 'btn-danger',
      iconClassName: 'fa-trash'
    }
  ];

  sizeButtonArray = [
    {
      title: 'Large button',
      className: 'btn-lg'
    },
    {
      title: 'Small button',
      className: 'btn-sm'
    },
  ];

  buttonArray = [
    {
      title: 'Primary',
      className: 'btn-primary',
      classNameOutline: 'btn-primary-outline'
    },
    {
      title: 'Secondary',
      className: 'btn-secondary',
      classNameOutline: 'btn-secondary-outline'
    },
    {
      title: 'Tertiary',
      className: 'btn-tertiary',
      classNameOutline: 'btn-tertiary-outline'
    },
    {
      title: 'Success',
      className: 'btn-success',
      classNameOutline: 'btn-success-outline'
    },
    {
      title: 'Warning',
      className: 'btn-warn',
      classNameOutline: 'btn-warn-outline'
    },
    {
      title: 'Danger',
      className: 'btn-danger',
      classNameOutline: 'btn-danger-outline'
    },
    {
      title: 'White',
      className: 'btn-white',
      classNameOutline: 'btn-white-outline'
    }
  ];

  constructor() {
    super();
  }

  ngOnInit() {
  }
}
