import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "../../../../utility/base-component/base.component";
import {BASE} from "../../../../utility/constants/base-constants";
import {SharedService} from "../../../../utility/shared.service";

enum View{
  STEP_ONE, STEP_TWO, STEP_THREE
}

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html'
})
export class StepperComponent extends BaseComponent implements OnInit {

  // State variables
  enumView = View;
  activeView : View = View.STEP_ONE;

  baseFile = new BASE();

  files = [
    {
      filePath: this.baseFile.FILE_PATH + 'stepper-files/basic-stepper.html',
      codeText: ''
    }, {
      filePath: this.baseFile.FILE_PATH + 'stepper-files/basic-stepper.js',
      codeText: ''
    }
  ];

  constructor(private _sharedService: SharedService) {
    super();
  }

  ngOnInit() {
    this.initializeMethod();
  }

  initializeMethod() {
    for (let i = 0; i < this.files.length; i++) {
      this._sharedService.onCopyFile(this.files[i].filePath).subscribe(res=> {
        this.files[i].codeText = res;
      });
    }
  }

  // Events
  onStepperChange(viewName : View) {
    this.activeView = viewName;
  }

}
