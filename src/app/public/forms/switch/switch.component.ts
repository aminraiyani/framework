import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "../../../../utility/base-component/base.component";

@Component({
  selector: 'app-switch',
  templateUrl: 'switch.component.html'
})
export class SwitchComponent extends BaseComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
