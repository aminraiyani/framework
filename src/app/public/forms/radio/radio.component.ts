import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "../../../../utility/base-component/base.component";

@Component({
  selector: 'app-radio',
  templateUrl: 'radio.component.html'
})
export class RadioComponent extends BaseComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }
}
