import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "../../../../utility/base-component/base.component";

@Component({
  selector: 'app-checkbox',
  templateUrl: 'checkbox.component.html'
})
export class CheckboxComponent extends BaseComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
