import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "../../../../utility/base-component/base.component";
import {SharedService} from "../../../../utility/shared.service";
import {BASE} from "../../../../utility/constants/base-constants";

@Component({
  selector: 'app-input-files',
  templateUrl: 'input-files.component.html'
})

export class InputFilesComponent extends BaseComponent implements OnInit {

  fileName: string;

  baseFile = new BASE();

  files = [
    {
      filePath: this.baseFile.FILE_PATH + 'file-selection-files/file-selection.html',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'file-selection-files/file-selection.js',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'file-selection-files/file-selection-button.html',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'file-selection-files/file-selection-right-button.html',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'file-selection-files/input-files-image.html',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'file-selection-files/input-files-image.js',
      codeText: ''
    }
  ];

  constructor(private _sharedService: SharedService) {
    super();
  }

  ngOnInit() {
    this.initializeMethod();
  }

  initializeMethod() {
    for (let i = 0; i < this.files.length; i++) {
      this._sharedService.onCopyFile(this.files[i].filePath).subscribe(res=> {
        this.files[i].codeText = res;
      });
    }
  }

  onFileSelect(id) {
    document.getElementById(id).click();
  }

  onFileGetName(event) {
    const file = event.target.files[0];
    this.fileName = file.name;
  }

  fileNameInitialize() {
    this.fileName = '';
  }
}
