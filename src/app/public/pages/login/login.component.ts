import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {BaseComponent} from "../../../../utility/base-component/base.component";
import {ValidationConstant, ValidationRegex} from "../../../../utility/constants/validations";
import {BASE} from "../../../../utility/constants/base-constants";
import {SharedService} from "../../../../utility/shared.service";

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html'
})
export class LoginComponent extends BaseComponent implements OnInit {

  baseFile = new BASE();

  files = [
    {
      filePath: this.baseFile.FILE_PATH + 'pages-files/basic-login-without-validation.html',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'pages-files/basic-login.html',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'pages-files/basic-login.js',
      codeText: ''
    },
    {
      filePath: this.baseFile.FILE_PATH + 'pages-files/color-login.html',
      codeText: ''
    }
  ];

  // Form variables
  loginForm: FormGroup;

  // validations message
  validationMsg = new ValidationConstant();

  constructor(private _sharedService: SharedService) {
    super();
  }

  ngOnInit() {
    this.initializeMethods();
  }

  initializeMethods() {
    this.createLoginForm();

    for (let i = 0; i < this.files.length; i++) {
      this._sharedService.onCopyFile(this.files[i].filePath).subscribe(res=> {
        this.files[i].codeText = res;
      });
    }
  }

  createLoginForm() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(ValidationRegex.EMAIL_ADDRESS_REGEXP),
        Validators.minLength(ValidationConstant.EMAIL_MIN_LENGTH),
        Validators.maxLength(ValidationConstant.EMAIL_MAX_LENGTH)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.pattern(ValidationRegex.PASSWORD_REGEXP),
        Validators.minLength(ValidationConstant.PASSWORD_MIN_LENGTH)
      ])
    });
  }

  onLoginFormSubmit(value, valid) {
    if (valid) {

    } else {

    }
  }
}
