export class APPStorage {
  public static Session_time = 'st';
  public static LoginAttempts = 'la';
  public static TOKEN = 'at';
  public static SERVER_TIME = 'ts';
  public static EMPLOYER_TOKEN = 'et';
  public static USER = 'u';
  public static REDIRECTED_FROM_REGISTER = 'rfr';
}
