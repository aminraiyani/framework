export class CommonStrings {
  public static LOADING_MESSAGE = 'Loading...';
  public static NO_DATA = 'No data found';
  public static Error_Occur = 'Something went wrong. Please try again';
}

