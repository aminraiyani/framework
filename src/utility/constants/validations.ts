export class ValidationRegex {
  /*Email*/
  public static EMAIL_ADDRESS_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  /*Password*/
  public static PASSWORD_REGEXP = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%^&+=])[a-zA-Z0-9@#$%^&+=]*$/;


  public static NUMERIC_REGEXP = '^[0-9]*$';
  public static NUMERIC_DOT_REGEXP = '^[0-9.]*$';
  public static CGPA_REGEXP = '^([1-3](\\.\\d{2}))$|([4](\\.[0]{2}))$';
  public static NUMERIC_DOT_REGEXP_LENGTH = '^[0-9]*(\.[0-9]{1,4})?$';
  public static ONLY_ALPHA_REGEXP = '^[a-zA-Z]*$';
  public static ONLY_ALPHA_SPACE_REGEXP = '^[a-zA-Z- ]*$';
  public static ALPHANUMERIC_REGEXP = '^[a-zA-Z0-9]*$';
  public static ALPHANUMERIC_SPACE_REGEXP = '^[a-zA-Z0-9 ]*$';
  public static PASSPORT_REGEXP = '^[a-zA-Z0-9]*$';
  public static SERVER_DATEFORMAT_REGEXP = '^\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])$';
  public static BITCOIN_REGEXP = '^[123mn][1-9A-HJ-NP-Za-km-z]{26,35}$';
  public static ALPHA_DOT_SPACE_REGEXP = '^[a-zA-Z. ]*$';
  public static ALPHA_DOT_REGEXP = '^[a-zA-Z.]*$';
  public static SKYPE_REGEXP = '[a-zA-Z][a-zA-Z0-9\.,\-_]{5,31}';
  // public static WEBSITE_REGEXP = '^.*$';
  public static WEBSITE_REGEXP = '(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})';
  public static HTTP_REGEX = /^((http|https):\/\/)/;
}

export class ValidationConstant {
  public IS_REQUIRED = ' is required';

  /*Email*/
  public static EMAIL_MIN_LENGTH = 6;
  public static EMAIL_MAX_LENGTH = 40;
  public EMAIL_REQUIRED = 'Email address' + this.IS_REQUIRED;
  public EMAIL_VALID = 'Enter valid email address';
  public EMAIL_LENGTH = 'email address length between' + ValidationConstant.EMAIL_MIN_LENGTH + ' to ' + ValidationConstant.EMAIL_MAX_LENGTH;


  /*Password*/
  public static PASSWORD_MIN_LENGTH = 8;
  public PASSWORD_REQUIRED = 'Password' + this.IS_REQUIRED;
  public PASSWORD_VALID = 'Enter valid password';
  public PASSWORD_LENGTH = 'Password minimum length is ' + ValidationConstant.PASSWORD_MIN_LENGTH;
}
