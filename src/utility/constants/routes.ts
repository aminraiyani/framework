export class RouteConstants {
  public static HOME = '';
  public static BUTTONS = 'buttons';
  public static TABLES = 'tables';
  public static INPUTS = 'inputs';
  public static INPUTS_FILES = 'inputs-files';
  public static SELECT = 'select';
  public static TEXTAREA = 'textarea';
  public static CHECKBOX = 'checkbox';
  public static RADIO = 'radio';
  public static SWITCH = 'switch';
  public static HEADER = 'header';
  public static SIDEBAR = 'sidebar';
  public static FOOTER = 'footer';
  public static MODAL = 'modal';
  public static ACCORDION = 'accordion';
  public static TABS = 'tabs';
  public static STEPPER = 'stepper';
  public static BORDERS = 'borders';
  public static FLOATS = 'floats';
  public static LOGIN = 'login';

  /*For library*/
  public static DASHBOARD = 'dashboard';
  public static ABOUT_US = 'about-us';
  public static CONTACT_US = 'contact-us';
  public static TERMS = 'terms-and-conditions';
  public static PRIVACY = 'privacy-policy';
}
