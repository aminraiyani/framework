export class BASE {
  public static PAGINATION_ARRAY: number[] = [20, 30, 40];
  public FILE_PATH = '../assets/code-files/';
}

export enum PopupState {
  Open,
  Close
}
