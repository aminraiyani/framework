import {BASE, PopupState} from './constants/base-constants';

declare var $;

export class CommonFunctions {

  public static isEmpty(obj): boolean {
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }
}

export function ClipboardText(value: string): boolean {
  try {
    const inp = document.createElement('input');
    document.body.appendChild(inp);
    inp.value = value;
    inp.select();
    document.execCommand('copy', false);
    inp.remove();
    return true;
  } catch (e) {
    return false;
  }
}

export function SetupScrollClass(popupState: PopupState) {
  popupState === PopupState.Open ? $("body").addClass("scroll-hide") : $("body").removeClass("scroll-hide");
}

export function SetUpScrollPosition() {
  $('#inputContainer').scroll(function () {
    //Set new top to autocomplete dropdown
    const newTop = $('#autocompleteInput').offset().top + $('#autocompleteInput').outerHeight();
    $('.pac-container').css('top', newTop + 'px');
  });
}
