import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'validation',
  templateUrl: './validation.component.html'
})
export class ValidationComponent implements OnInit {

  @Input() errMsg: string;

  constructor() { }

  ngOnInit() {

  }

}
