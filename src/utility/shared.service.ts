import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {HttpClient} from "@angular/common/http";
// import any = jasmine.any;

@Injectable()
export class SharedService {

  constructor(private http: HttpClient) {
  }

  onCopyFile(file: string): Observable<any> {
    return this.http.get(file, {responseType: 'text'})
  }

}
