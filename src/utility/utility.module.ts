import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BaseComponent} from './base-component/base.component';
import {DisableControlDirective} from './directives/disable-control.directive';

import {AutofocusDirective} from './directives/focus.directive';
import {ValidMobileDirective} from './directives/valid-mobile-checks.directive';
import {AngularFontAwesomeModule} from "angular-font-awesome";
import {SafeHtmlPipe} from "./pipes/safe-html.pipe";
import {SharedService} from "./shared.service";
import {ValidationComponent} from "./validation/validation.component";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule
  ],
  declarations: [
    AutofocusDirective,
    BaseComponent,
    ValidMobileDirective,
    ValidationComponent,
    DisableControlDirective,
    SafeHtmlPipe
  ],
  providers: [SharedService],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    AutofocusDirective,
    BaseComponent,
    ValidationComponent,
    ValidMobileDirective,
    DisableControlDirective,
    SafeHtmlPipe
  ]
})

export class UtilityModule {
}
