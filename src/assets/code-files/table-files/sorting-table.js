// State variables
sortKey = 'username';
isSort = false;

// Events
onSorting(keyName) {
  this.sortKey === keyName ? (this.isSort = !this.isSort) : this.isSort = true;
  this.sortKey = keyName;
}
