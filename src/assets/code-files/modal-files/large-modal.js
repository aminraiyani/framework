/*state variables*/
largeModal = false;

/*Events*/
onShowBasicModal(id) {
  this.largeModal = true;
  this.onOpenModal(id);
}

onCloseModal() {
  this.onRemoveScroll();
  this.largeModal = false;
}

/*Helpers*/
@HostListener('window:keydown', ['$event'])
keyboardInput(event: any) {
  if (event.keyCode === 27) {
    this.onCloseModal();
  }
}
