/*state variables*/
basicModal = false;

/*Events*/
onShowBasicModal(id) {
  this.basicModal = true;
  this.onOpenModal(id);
}

onCloseModal() {
  this.onRemoveScroll();
  this.basicModal = false;
}

/*Helpers*/
@HostListener('window:keydown', ['$event'])
keyboardInput(event: any) {
  if (event.keyCode === 27) {
    this.onCloseModal();
  }
}
