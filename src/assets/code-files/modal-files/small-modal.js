/*state variables*/
smallModal = false;

/*Events*/
onShowBasicModal(id) {
  this.smallModal = true;
  this.onOpenModal(id);
}

onCloseModal() {
  this.onRemoveScroll();
  this.smallModal = false;
}

/*Helpers*/
@HostListener('window:keydown', ['$event'])
keyboardInput(event: any) {
  if (event.keyCode === 27) {
    this.onCloseModal();
  }
}
