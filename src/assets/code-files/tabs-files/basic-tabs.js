enum View {
  TAB_ONE, TAB_TWO, TAB_THREE
}

// State variables
enumView = View;
activeView : View = View.TAB_ONE;

// Events
onOpenTab(viewName : View){
  this.activeView = viewName;
}
