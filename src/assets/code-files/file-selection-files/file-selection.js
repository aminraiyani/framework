/*state variables*/
fileName: string;

/*events*/
onFileSelect(id){
  document.getElementById(id).click();
}

onFileGetName(event){
  const file = event.target.files[0];
  this.fileName = file.name;
}
