constructor(private router: Router){
}

/*Helpers*/
get aboutUsUrl(){
  return ['/' + RouteConstants.ABOUT_US];
}

get contactUsUrl(){
  return ['/' + RouteConstants.CONTACT_US];
}

get termsUrl(){
  return ['/' + RouteConstants.TERMS];
}

get policyUrl(){
  return ['/' + RouteConstants.PRIVACY];
}
