/*State variables*/
isOpenMenu : any = false;

constructor(private router: Router){
}

/*Events*/
onOpenMenu(name) {
  this.isOpenMenu === name ? this.isOpenMenu = '' : this.isOpenMenu = name;
}

/*Helpers*/
get dashboardUrl(){
  return ['/' + RouteConstants.DASHBOARD];
}
