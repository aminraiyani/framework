/*State Variables*/
isExpand = false;

constructor(private router: Router){
}

/*Events*/
menuToggle() {
  this.isExpand = !this.isExpand;
}

/*Helpers*/
get dashboardUrl(){
  return ['/' + RouteConstants.DASHBOARD];
}
