/*State variables*/
isSidebarMove = false;

constructor(private router: Router){
}

/*Events*/
hideMenu() {
  this.isSidebarMove = false;
}

onSidebarHover() {
  this.isSidebarMove = true;
}

/*Helpers*/
get dashboardUrl(){
  return ['/' + RouteConstants.DASHBOARD];
}
