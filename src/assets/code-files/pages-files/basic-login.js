// Form variables
loginForm: FormGroup;

// validations message
validationMsg = new ValidationConstant();

ngOnInit() {
  this.initializeMethods();
}

initializeMethods() {
  this.createLoginForm();
}

createLoginForm() {
  this.loginForm = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern(ValidationRegex.EMAIL_ADDRESS_REGEXP),
      Validators.minLength(ValidationConstant.EMAIL_MIN_LENGTH),
      Validators.maxLength(ValidationConstant.EMAIL_MAX_LENGTH)
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.pattern(ValidationRegex.PASSWORD_REGEXP),
      Validators.minLength(ValidationConstant.PASSWORD_MIN_LENGTH)
    ])
  });
}

onLoginFormSubmit(value, valid) {
  if (valid) {

  } else {

  }
}
