enum View{
  STEP_ONE, STEP_TWO, STEP_THREE
}

// State variables
enumView = View;
activeView : View = View.STEP_ONE;

// Events
onStepperChange(viewName : View) {
  this.activeView = viewName;
}
